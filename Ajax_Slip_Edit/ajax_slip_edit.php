<?php

include '../1Connection.php';
include '../main_function/login.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

$query = "SELECT * FROM [dbo].[Receive] 
WHERE ARCHIVE = '0' 
AND SLIP_EDIT IS NULL
ORDER BY DATE_RECEIVE DESC, id DESC";
$result = sqlsrv_query($conn, $query);

?>

<div id="swal_content"></div>

<div class="container" style="padding:20px;">

    <div class="Col-2-30" style="margin-right:50px;">

        <h2>Edit Form</h2>

        <div style="margin-bottom:20px;">
            <span class="receive_label"> ENTER INVOICE: </span> <br>
            <input type="text" class="receive_input" id="search_edit_invoice" value="" 
            style="text-align:center;">
        </div>

        <div style="margin-bottom:20px;">
            <span class="receive_label"> PURCHASE SLIP: </span> <br>
            <input type="text" class="receive_input" id="new_p_slip" value="IM<?php echo $currentYear . $currentMonth ?>-000"
            style="text-align:center;">
        </div>

        <div style="margin-bottom:20px;">
            <span class="receive_label"> SALES SLIP: </span> <br>
            <input type="text" class="receive_input" id="new_s_slip" value="INV<?php echo $currentYear . $currentMonth ?>-000" 
            style="text-align:center;">
        </div>

        <div class="margin_bottom">
            <button class="receive_button" id="submit_slip_edit"><i class="fa-solid fa-cloud-arrow-up"></i> SUBMIT</button>
        </div>

    </div>

    <div class="Col-1-70">

        <h2>Unfilled Sales List And Purchase Slip</h2>

        <div id="slip_search_table" class="close"></div>

        <table id="slip_default_table" class="table w3-table-all w3-hoverable ui striped table">

            <thead class="report_table_head">
                <tr>
                    <th class="close">status</th>
                    <th>invoice</th>
                    <th>P SLIP</th>
                    <th>S SLIP</th>
                    <th>goods code</th>
                    <th>quantity</th>
                    <th class="close">P.O</th>
                    <th>item code</th>
                    <th class="close">materials type</th>
                    <th class="close">part number</th>
                    <th class="close">date receive</th>
                    <th class="close">action</th>
                </tr>
            </thead>

            <tbody class="report_table_body">
                    
                <?php

                    while($rows=sqlsrv_fetch_array($result)){

                        $color = "#fff";
                        // $color = "#000";

                        if($rows['DATE_RECEIVE']->format('m') == '01'){
                            $bg = "#0000FF";
                            // $bg = "#5265B2";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '02'){
                            $bg = "#8F00FF";
                            // $bg = "#C199CE";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '03'){
                            // $bg = "orange";
                            $bg = "#F47F39";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '04'){
                            // $bg = "gray";
                            $bg = "#A2B2AC";
                            $color = "#000";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '05'){
                            $bg = "#4C9A2A";
                            // $bg = "#7DB166";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '06'){
                            // $bg = "black";
                            $bg = "#0D0C12";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '07'){
                            $bg = "#FFC0CB";
                            $color = "#000";
                            // $bg = "#E9B7C3";
                            // $color = "#000";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '08'){
                            $bg = "#964B00";
                            // $bg = "#9E8748";
                            // $color = "#000";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '09'){
                            // $bg = "yellow";
                            $bg = "#FED758";
                            $color = "#000";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '10'){
                            $bg = "#7AD7F0";
                            // $bg = "#9AB9D6";
                            $color = "#000";
                        }
                        elseif($rows['DATE_RECEIVE']->format('m') == '11'){
                            // $bg = "#fff";
                            $bg = "#FFFDFA";
                            $color = "#000";
                        }
                        else{
                            $bg = "#880808";
                            // $bg = "#DB937B";
                        }


                        if($rows['STATUS'] == 'RAW'){
                            $status = '#880808';
                        }
                        else{
                            $status = 'green';
                        }
                                                                
                        echo "<tr style='color:".$color."; background:".$bg.";'>
                                <td class='close' style='color:#fff; background:".$status.";'>" . $rows['STATUS'] . "</td>
                                <td>" . $rows['INVOICE'] . "</td>
                                <td>" . $rows['P_SLIP'] . "</td>
                                <td>" . $rows['S_SLIP'] . "</td>
                                <td>" . $rows['GOODS_CODE'] . "</td>
                                <td>" . $rows['QTY'] . "</td>
                                <td>" . $rows['ITEM_CODE'] . "</td>
                                <td class='close'>
                                    <button class='report_table_action edit_desktop' id=desktop_edit". $rows['id'] ."><i class='fa-solid fa-pen-to-square'></i> EDIT</button> 
                                    <button class='report_table_action delete_desktop' id=desktop_delete". $rows['id'] ."><i class='fa-solid fa-trash'></i> DELETE</button>
                                </td>
                            </tr>";
                    }

                ?>

            </tbody>

        </table> 
    
    </div>
        
</div>

<script>

    var s_slip = '';
    var invoice = '';
    var p_slip = '';

    var search_invoice = '';

    jQuery('#search_edit_invoice').on("input", function() {
        search_invoice = jQuery(this).val(); //or this.value
        // console.log(search_invoice);

        if ( search_invoice != '' ) {

            $("#slip_default_table").addClass("close");
            $("#slip_search_table").removeClass("close");

            $.ajax({
                type: "POST",
                url: '../Ajax_Slip_Edit/ajax_search_invoice.php',
                data: {
                    search_invoice: search_invoice
                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    // $("#QR-Code").val('');
                    // $("#quantity_input").val('');
                    // $("#invoice_input").val('');

                    $("#slip_search_table").html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }

            });
        }
        else {

            $("#slip_default_table").removeClass("close");
            $("#slip_search_table").addClass("close");

        }
    });

    // jQuery('#new_p_slip').blur(function () {
    //     p_slip = jQuery(this).val(); //or this.value
    //     console.log(p_slip);
    // });

    // jQuery('#new_s_slip').blur(function () {
    //     s_slip = jQuery(this).val(); //or this.value
    //     console.log(s_slip);
    // });


    $("#submit_slip_edit").click(function() {
 
        var invoice = $("#search_edit_invoice").val();
        var sales_slip = $("#new_s_slip").val();
        var purchase_slip = $("#new_p_slip").val();
        console.log(invoice);
        console.log(purchase_slip);
        console.log(sales_slip);


        $.ajax({
            type: "POST",
            url: '../Ajax_Slip_Edit/ajax_update_slip.php',
            data: {
                invoice: invoice,
                purchase_slip: purchase_slip,
                sales_slip: sales_slip

            },
            cache: false,
            success: function(data) {
                $("#swal_content").html(data);
                // alert(data);
                // $("#QR-Code").val('');
                // $("#quantity_input").val('');
                // $("#invoice_input").val('');

                // $(".ajax_table_desktop").html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    });

</script>