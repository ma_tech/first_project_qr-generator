<?php

include '../1Connection.php';
include '../main_function/login.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$search = $_POST['search'] ?? '';
$month = $_POST['month'] ?? '';

trim($month);
trim($search);

if($month == 'ALL' && $search != ''){
    $query = "SELECT * FROM [dbo].[Receive] 
    WHERE CHARINDEX ('$search', GOODS_CODE) > 0 
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', ITEM_CODE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', INVOICE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', DATE_RECEIVE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', ASY_LINE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', SUPPLIER) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', PART_NUMBER) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', PO) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    OR CHARINDEX ('$search', STATUS) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    ORDER BY DATE_RECEIVE DESC,
    id DESC";
    $result = sqlsrv_query($conn, $query);
}

elseif ($month == 'ALL' && $search == NULL) {
    $query = "SELECT * FROM [dbo].[Receive] 
    WHERE INVOICE != 'STARTING'
    AND ARCHIVE = '0'
    ORDER BY DATE_RECEIVE DESC,
    id DESC";
    $result = sqlsrv_query($conn, $query);
}

elseif ($month != 'ALL' && $search == '') {

    if ($month == "01" || 
        $month == "03" || 
        $month == "05" || 
        $month == "07" || 
        $month == "08" || 
        $month == "10" || 
        $month == "12") {

        $EndDayOfTheMonth = "31";

    }

    elseif( $month == "04" || 
            $month == "06" || 
            $month == "09" || 
            $month == "11") {

            $EndDayOfTheMonth = "30";

    }

    else {

        $EndDayOfTheMonth = "28";

    }

    $query = "SELECT * FROM [dbo].[Receive] 
    WHERE ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    ORDER BY DATE_RECEIVE DESC,
    id DESC";
    $result = sqlsrv_query($conn, $query);
}

else{

    if ($month == "01" || 
        $month == "03" || 
        $month == "05" || 
        $month == "07" || 
        $month == "08" || 
        $month == "10" || 
        $month == "12") {

        $EndDayOfTheMonth = "31";

    }

    elseif( $month == "04" || 
            $month == "06" || 
            $month == "09" || 
            $month == "11") {

            $EndDayOfTheMonth = "30";

    }

    else {

        $EndDayOfTheMonth = "28";

    }

    $query = "SELECT * FROM [dbo].[Receive] 
    WHERE CHARINDEX ('$search', GOODS_CODE) > 0 
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', ITEM_CODE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', INVOICE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', DATE_RECEIVE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', ASY_LINE) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', SUPPLIER) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', PART_NUMBER) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', PO) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    OR CHARINDEX ('$search', STATUS) > 0
    AND ARCHIVE = '0' 
    AND INVOICE != 'STARTING'
    AND DATE_RECEIVE >= '$currentYear-$month-01'
    AND DATE_RECEIVE <= '$currentYear-$month-$EndDayOfTheMonth'
    ORDER BY DATE_RECEIVE DESC,
    id DESC";
    $result = sqlsrv_query($conn, $query);
}

?>

<table id="search_report">

    <thead class="report_table_head">
        <tr>
            <th>status</th>
            <th>PURCHASE SLIP</th>
            <th>SALES SLIP</th>
            <th>goods code</th>
            <th>invoice</th>
            <th>quantity</th>
            <th>P.O</th>
            <th>item code</th>
            <th>assy line</th>
            <th>supplier</th>
            <th>ms21 part number</th>
            <th>supplier part number</th>
            <th>part name</th>
            <th>date receive</th>
        </tr>
    </thead>

    <tbody class="report_table_body">
            
        <?php

            $stoper = 0;

            while($rows=sqlsrv_fetch_array($result)){

                $color = "#fff";

                if($rows['DATE_RECEIVE']->format('m') == '01'){
                        $bg = "#0000FF";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '02'){
                    $bg = "#8F00FF";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '03'){
                    $bg = "#F47F39";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '04'){
                    $bg = "#A2B2AC";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '05'){
                    $bg = "#4C9A2A";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '06'){
                    $bg = "#0D0C12";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '07'){
                    $bg = "#FFC0CB";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '08'){
                    $bg = "#964B00";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '09'){
                    $bg = "#FED758";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '10'){
                    $bg = "#7AD7F0";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '11'){
                    $bg = "#FFFDFA";
                    $color = "#000";
                }
                else{
                    $bg = "#880808";
                }

                // if($rows['STATUS'] == 'RAW'){
                //     $status = '#880808';
                // }
                // else{
                //     $status = 'green';
                // }
                                                        
                echo "<tr style='color:".$color."; background:".$bg.";'>
                        <td style='color:#fff; background:".$status.";'>" . $rows['STATUS'] . "</td>
                        <td>" . $rows['P_SLIP'] . "</td>
                        <td>" . $rows['S_SLIP'] . "</td>
                        <td>" . $rows['GOODS_CODE'] . "</td>
                        <td>" . $rows['INVOICE'] . "</td>
                        <td>" . number_format($rows['QTY']) . "</td>
                        <td>" . $rows['PO'] . "</td>
                        <td>" . $rows['ITEM_CODE'] . "</td>
                        <td>" . $rows['ASY_LINE'] . "</td>
                        <td>" . $rows['SUPPLIER'] . "</td>
                        <td>" . $rows['PART_NUMBER'] . "</td>
                        <td>" . $rows['SUPPLIER_PART_NUMBER'] . "</td>
                        <td>" . $rows['PART_NAME'] . "</td>
                        <td>" . $rows['DATE_RECEIVE']->format('Y-m-d') . "</td>
                    </tr>";

                if($rows['INVOICE'] == $search ){

                    $queryINV = "SELECT SUM (QTY) as Total_Sum
                    FROM [dbo].[Receive]
                    WHERE INVOICE = '$search'
                    AND ARCHIVE = '0'
                    GROUP BY INVOICE";
                    $resultINV = sqlsrv_query($conn, $queryINV);

                    while($rows=sqlsrv_fetch_array($resultINV)){

                        if($stoper == 0){
                            echo "<span class='sum_label'>Total Quantity For This Invoice:</span> <strong class='sum'>" . $rows['Total_Sum'] . "</strong>";
                            $stoper = 1;
                        }
                        
                    }

                }

            }

        ?>

    </tbody>

</table>

<!-- SCRIPT FOR DATATABLE -->
<script>
    $('#search_report').DataTable({ 
        "ordering": false,
        "scrollX": true,
        // "lengthChange": false, // disable the length 10/25/50/100
        "pagingType": "simple_numbers", //paging type numbers/simple/simple_numbers/full/full_numbers/first_last_numbers
        // "pageLength": 10,
        "searching": false
    });
</script>