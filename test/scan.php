<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/minified/html5-qrcode.min.js"></script>
    <script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>

    <!-- <script src="https://salingsub.com/html5-qrcode.min.js" type="text/javascript"></script> -->

</head>

    <body>

    <div id="reader" width="600px"></div>
    <div id="result"></div>

    <div class="form_part"> 
        <input type="text" value="" id="QR-Code" name="QR-Code" placeholder="QR Detected" readonly>
    </div>

    
    <script>
        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete"
            || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } 
            else {
                document.addEventListener("DOMContentLoaded", fn);
                //document.getElementById(‘text’).value=c;
            }
        }

        docReady(function () {
            var resultContainer = document.getElementById('result');
            var lastResult, countResults = 0;
            function onScanSuccess(decodedText, decodedResult) {
                if (decodedText !== lastResult) {
                    ++countResults;
                    lastResult = decodedText;
                    // Handle on success condition with the decoded message.
                    console.log('Scan result ${decodedText}', decodedResult);
                    document.getElementById("result").textContent=lastResult;
                    document.getElementById('QR-Code').value = lastResult;

                    var shutter = new Audio();
                    shutter.autoplay = true;
                    shutter.src = navigator.userAgent.match(/Firefox/) ? 'audio/read.mp3' : 'audio/read.ogg';

                }

            }

        var html5QrcodeScanner = new Html5QrcodeScanner(
        "reader", { fps: 10, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);

        });
    </script>

    </body>

</html>