<?php


include '../1Connection.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');

$currentDate = $currentYear . "-" . $currentMonth . "-" . $currentDay;

$goods_code_value = $_POST['goods_code_value'] ?? '';
$supplier_value = $_POST['supplier_value'] ?? '';
$item_code_value = $_POST['item_code_value'] ?? '';
$material_value = $_POST['material_value'] ?? '';
$part_num_value = $_POST['part_num_value'] ?? '';
$goods_name_value = $_POST['goods_name_value'] ?? '';
$part_name_value = $_POST['part_name_value'] ?? '';
$assy_line_value = $_POST['assy_line_value'] ?? '';
$quantity = $_POST['quantity'] ?? '';
$invoice = $_POST['invoice'] ?? '';
$purchase_slip = $_POST['purchase_slip'] ?? '';
$sales_slip = $_POST['sales_slip'] ?? '';
$location_value = $_POST['location_value'] ?? '';
$archieve = 0;
$po = $_POST['po'] ?? '';
// Traceability QR Code Generator
$totalNumberOfBox = $_POST['totalNumberOfBox'] ?? '';
// $bsn_id = $_POST['bsn_id'] ?? '';


$query = sqlsrv_query( $conn, "SELECT * FROM [Receive] 
WHERE INVOICE ='$invoice'
AND DATE_RECEIVE = '$currentDate'
AND GOODS_CODE = '$goods_code_value'
AND ITEM_CODE = '$item_code_value'
AND QTY = '$quantity'
AND PO = '$po'
AND ARCHIVE = '0'", array());


// $query = sqlsrv_query( $conn, "SELECT * FROM [Receive] 
// WHERE GOODS_CODE = '$goods_code_value'
// AND ITEM_CODE ='$item_code_value'
// AND DATE_RECEIVE = '$currentDate'", array());


if ($query !== NULL) {  


    $rows = sqlsrv_has_rows( $query );  

    if ($rows === true) {
        
        ?>
            <script>

                var goods_code = '<?php echo $goods_code_value; ?>';
                var item_code = '<?php echo $item_code_value; ?>';
                var invoice = '<?php echo $invoice; ?>';
                var quantity = '<?php echo $quantity; ?>';


                if (confirm('The same Goods Code, Quantity, Item Code, and Invoice are already in our database. Are you sure you want to go ahead with this transaction because it may result in a double entry?')) {
                    
                    $.ajax({
                        type: "POST",
                        url: '../Ajax_Scanner/update_same.php',
                        data: {
                            goods_code: goods_code,
                            item_code: item_code,
                            invoice: invoice,
                            quantity: quantity
                            
                        },
                        cache: false,
                        success: function(data) {
                            alert(data);
                            // $("#QR-Code").val('');
                            // $("#quantity_input").val('');
                            // $("#invoice_input").val('');

                        },
                        error: function(xhr, status, error) {
                            console.error(xhr);
                        }

                    });

                } else {
                    
                }
              </script>
        <?php

    }

    else{
        // Traceability QR Code Generator
        if ($quantity === '' || $invoice === '' || $totalNumberOfBox === ''){
            
            echo "<script language='javascript'>
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'We can not process this transaction because the QUANTITY, INVOICE or NUMBER OF BOX is empty.',
                    })
                </script>";
            return;
        }

        $query2 = sqlsrv_query( $conn, "SELECT * FROM [Receive] 
        WHERE INVOICE ='$invoice'
        AND DATE_RECEIVE = '$currentDate'
        AND GOODS_CODE = '$goods_code_value'
        AND ITEM_CODE = '$item_code_value'
        AND po = '$po'

        AND ARCHIVE = '0'", array());

        if($query2 === false) {
            die( print_r( sqlsrv_errors(), true) );
            }
        
        $rows2 = sqlsrv_has_rows( $query2 );
        if($rows2 == true){
            while($rowData = sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)) {
                // echo "<script>alert('". $rowData['PO']."')</script>";
                 // if($rowData[])
                $updatedQTY = $rowData['QTY'] + $quantity;
                $updated_NUMBER_OF_BOX = $rowData['NUMBER_OF_BOX'] + $totalNumberOfBox;
                $rowID = $rowData['id'];
                $updateRowData = "UPDATE [MA_Receiving].[dbo].[Receive] SET QTY = '$updatedQTY', NUMBER_OF_BOX = '$updated_NUMBER_OF_BOX' WHERE id = '$rowID';";
                $result = sqlsrv_query($conn, $updateRowData);
                if($result){
                    echo "<script language='javascript'>
                            Swal.fire({
                                icon: 'success',
                                title: 'Received!',
                                text: 'Upload was successful!',
                            })
                        </script>";
                }
             }
             return;
        }
        // Traceability QR Code Generator
        else
        {
            $sql = "SELECT * FROM [Receive]";
            $params = array();
            $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
            $stmt = sqlsrv_query( $conn, $sql , $params, $options );

            $row_count = sqlsrv_num_rows( $stmt );

            if ($row_count === false){
                echo 'Error in retrieveing row count.';
            } 

            else{

                $new_row_count = $row_count + 1;

                if ($quantity === '' || $invoice === '' || $totalNumberOfBox === ''){
                    //echo "Machine ID is EMPTY.";
                    // echo 'Error: We can not process this transaction because the QUANTITY or P.O is empty.';
                    echo "<script language='javascript'>
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'We can not process this transaction because the QUANTITY, INVOICE or NUMBER OF BOX is empty.',
                            })
                        </script>";
                }
                else{
                    // Traceability QR Code Generator
                    // Box Serial Number Start

                    $dateToday = date('Y-m-d');
                    // $dateToday = "2023-05-01";
                    $yearToday = date('y', strtotime($dateToday));
                    $monthToday = date('m', strtotime($dateToday));
                    $serialFirstDigits = $yearToday . $monthToday;
                    // $dateToday = "2023-03-01"; // date to check
                    $yesterday = date_create($dateToday)->modify('-1 days')->format('Y-m-d'); // get yesterday's date


                    $BoxSerialNoQuery = "SELECT MAX([BOX_SERIAL_NO]) AS last_id_number
                    FROM [MA_Receiving].[dbo].[Receive]";

                    $queryResult = sqlsrv_query($conn, $BoxSerialNoQuery);

                    while($boxRows=sqlsrv_fetch_array($queryResult)){
                        if($boxRows['last_id_number'] == NULL || $boxRows['last_id_number'] == ""){
                            $lastBsn_id = NULL;
                        }else{
                            $lastBsn_id = explode("-",$boxRows['last_id_number']);
                        }
                        
                    }

                    if($lastBsn_id == NULL){
                        $serialSecondDigits = 1;
                    }
                    else {
                        if(date('m', strtotime($yesterday)) != date('m', strtotime($dateToday))) {
                            // echo "Date has been reset to next month";
                            // echo "<br>";
                            // echo date('y', strtotime($dateToday)).date('m', strtotime($dateToday));
                            if($serialFirstDigits == $lastBsn_id[0] && $lastBsn_id[1] >= 1){
                                $serialSecondDigits = $lastBsn_id[1] + 1;
                            }
                            else {
                                $serialSecondDigits = 1;
                            }
                        }
                        else {
                            // echo "Date is still in the same month";
                            $serialSecondDigits = $lastBsn_id[1] + 1;
                        }
                    }

                    $serialSecondDigits = str_repeat('0', 5 - strlen($serialSecondDigits)).$serialSecondDigits ;
                    // echo "<br>";
                    $bsn_id = $serialFirstDigits . "-" . $serialSecondDigits;
                    // Box Serial Number End
                    // Traceability QR Code Generator

                    $tsql = "INSERT INTO [Receive] 
                    (GOODS_CODE, ASY_LINE, SUPPLIER, MATERIALS_TYPE, ITEM_CODE, PART_NUMBER, PART_NAME, QTY, DATE_RECEIVE, INVOICE, P_SLIP, S_SLIP, ARCHIVE, QTY_S, STATUS, PO, NUMBER_OF_BOX, BOX_SERIAL_NO, SUPPLIER_PART_NUMBER, ACTUAL_RECEIVE_DATE)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                
                    $params1 = array(
                        $goods_code_value, 
                        $assy_line_value, 
                        $supplier_value, 
                        $material_value, 
                        $item_code_value, 
                        $part_num_value, 
                        $part_name_value, 
                        $quantity, 
                        $currentDate,
                        $invoice,
                        $purchase_slip,
                        $sales_slip,
                        $archieve,
                        $quantity,
                        'RAW',
                        $po,
                        $totalNumberOfBox,
                        $bsn_id,
                        $goods_name_value,
                        $currentDate);
                
                    // $stmt1 = sqlsrv_query( $conn, $tsql, $params1);
                    $stmt1 = sqlsrv_query( $conn, $tsql, $params1);
                    
                    if( $stmt1 ){

                        // echo "<div style='text-align:center; margin:10px 0px;'>Upload was successful.</div>";
                        echo "<script language='javascript'>
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Received!',
                                    text: 'Upload was successful!',
                                    html: '<h4>Box Serial No:</h4><h5 style=\"color:red;\">".$bsn_id."</h5>'
                                })
                            </script>";
                        
                    }
                                                    
                    else
                    {
                        // echo 'The upload failed.';
                        echo "<script language='javascript'>
                            Swal.fire({
                                icon: 'error',
                                title: 'Error!',
                                text: 'Dial the developer right now! ".print_r( sqlsrv_errors(), true)."',
                            })
                        </script>";
                        // die( print_r( sqlsrv_errors(), true));
                    }
                }
            }
        }
    }

}


?>