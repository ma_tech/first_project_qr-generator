<?php


include '../1Connection.php';

$Code = $_POST['Code'] ?? '';

// $query = "SELECT * FROM POLISTOrigdownloadToss
// WHERE CHARINDEX('$Code', [Slip No]) > 0
// AND [P/O balance qty] > '0'";
$query = "SELECT * FROM POLISTOrigdownloadToss
WHERE CHARINDEX('$Code', [Slip No]) > 0";
$result = sqlsrv_query($conn_ms21_dup, $query);

$counter = 0;

date_default_timezone_set('Asia/Manila');
$time = date('Y-m-d h:i:s A');

// echo $time;

while($rows=sqlsrv_fetch_array($result)){

    $gc[$counter] = $rows['Goods code'];
    $toss_po[$counter] = $rows['Slip No'];
    $po_bal[$counter] = $rows['P/O balance qty'];
    $po_qty[$counter] = $rows['P/O qty'];
    $vender_name[$counter] = $rows['Vender name'];
    $goods_name[$counter] = $rows['Goods name'];
    $goods_name_2[$counter] = $rows['Goods name2'];
    
    if ($rows['Req# dlv'] == NULL){
        $delivery_date[$counter] = "N/A";
    }
    else {
        $delivery_date[$counter] = $rows['Req# dlv']->format('Y-m-d');
    }

    if ($rows['E/U P/O No'] == NULL){
        $ms_po[$counter] = "N/A";
    }
    else {
        $ms_po[$counter] = $rows['E/U P/O No'];
    }

    $counter++;

}




if ($counter == '0'){

    // echo "<div style='text-align:center; margin:10px 0px;'>P.O is missing from our database.</div>";
    echo "<script language='javascript'>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'P.O is missing from our database.',
            })
        </script>";

}

else {

    echo "<div style='text-align:center; margin:10px 0px;'>P.O RESULT: " . $counter . "</div>";

    $count = 1;
    
    // echo "<th>G Code</th>";
    // echo "<th>TOSS PO</th>";
    // echo "<th>PO QTY</th>";
    // echo "<th>PO BAL.</th>";
    // echo "<th>ASSY</th>";

    for($x = 0; $x <= $counter; $x++){

        $query1 = "SELECT * FROM [MS21_MASTER_LIST]
        WHERE GOODS_CODE = '$gc[$x]'";
        $result = sqlsrv_query($conn, $query1);

        while($rows=sqlsrv_fetch_array($result)){

            echo "<table style='margin-bottom:10px;'>";
            echo "<tr style='width:100%; text-align:center;'>
                    <td id='goods_code".$count."' style='width:20%;'>".$rows['GOODS_CODE']."</td>
                    <td id='po".$count."' style='width:20%; display:none;'>".$toss_po[$x]."</td>
                    <td style='width:20%;'>".$goods_name[$x]."</td>
                    <td id='ms_po".$count."' style='width:20%; display:none;'>".$ms_po[$x]."</td>
                    <td id='d_date".$count."' style='width:20%; display:none;'>".$delivery_date[$x]."</td>
                    <td id='po_qty".$count."' style='width:20%;'>".number_format($po_qty[$x])."</td>
                    <td id='po_bal".$count."' style='width:20%;'>".number_format($po_bal[$x])."</td>
                    <td id='assy".$count."' style='width:20%;'>".$rows['ASSY_LINE']."</td>
                    <td id='vender_name".$count."' style='width:20%; display:none;'>".$vender_name[$x]."</td>
                    <td id='goods_name".$count."' style='width:20%; display:none;'>".$goods_name[$x]."</td>
                    <td id='goods_name_2".$count."' style='width:20%; display:none;'>".$goods_name_2[$x]."</td>
                    <td style='width:20%;'>
                        <button 
                            id='".$count."'
                            onClick='picked_click(this.id)'
                            style='
                                color: #fff; 
                                background: #363795;
                                border: none;
                                outline: none;
                                padding: 5px 10px;
                                border-radius: 5px;' 
                            ><i class='fa-solid fa-circle-info'></i> Info
                        </button>
                    </td>
                </tr>";
                echo "</table>";
            
            $count++;

        }

        $count++;

    }

}

?>

<script>

    function picked_click(clicked_id){

        id = clicked_id;

        var Code = $('#goods_code'+id).text();
        var Assy = $('#assy'+id).text();
        var Po = $('#po'+id).text();
        var MS_Po = $('#ms_po'+id).text();
        var D_Date = $('#d_date'+id).text();
        var Po_Bal = $('#po_bal'+id).text();
        var Po_Qty = $('#po_qty'+id).text();
        var Vender_name = $('#vender_name'+id).text();
        var Goods_name = $('#goods_name'+id).text();
        var Goods_name_2 = $('#goods_name_2'+id).text();

        console.log(Vender_name);

        $.ajax({
            type: "POST",
            url: '../Ajax_Scanner/ajax_table.php',
            data: {
                    Code: Code,
                    Assy: Assy,
                    Po: Po,
                    MS_Po: MS_Po,
                    D_Date: D_Date,
                    Po_Bal: Po_Bal,
                    Po_Qty: Po_Qty,
                    Vender_name: Vender_name,
                    Goods_name : Goods_name,
                    Goods_name_2 : Goods_name_2
            },
            cache: false,
            success: function(data) {
                $(".ajax_table_mobile").html(data);
                $(".ajax_table_desktop").html(data);
                $("#ajax_search_result").removeClass("close");
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    }

</script>
