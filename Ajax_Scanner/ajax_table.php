<?php 

include '../1Connection.php';

$Code = $_POST['Code'] ?? '';
$Assy = $_POST['Assy'] ?? '';
$Po = $_POST['Po'] ?? '';
$MS_Po = $_POST['MS_Po'] ?? '';
$D_Date = $_POST['D_Date'] ?? '';
$Po_Bal = $_POST['Po_Bal'] ?? '';
$Po_Qty = $_POST['Po_Qty'] ?? '';
$Vender_name = $_POST['Vender_name'] ?? '';
$Goods_name = $_POST['Goods_name'] ?? '';
$Goods_name_2 = $_POST['Goods_name_2'] ?? '';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

// ORIGINAL QUERY
// $query = "SELECT * FROM [dbo].[Masterlist] 
// WHERE PRODUCT_COAT = '$Code'
// OR  PRODUCT_KEY = '$Code'
// OR ABOLITION = '$Code'
// OR PRODUCT_NAME = '$Code'";
// $result = sqlsrv_query($conn, $query);

// $query = "SELECT * FROM [dbo].[Masterlist] 
// WHERE CHARINDEX('$Code', PRODUCT_NAME) > 0
// OR CHARINDEX('$Code', PRODUCT_COAT) > 0";
// $result = sqlsrv_query($conn, $query);

$query = "SELECT * FROM MS21_MASTER_LIST
WHERE CHARINDEX('$Code', UPDATED_PRODUCT_NAME) > 0
OR CHARINDEX('$Code', GOODS_CODE) > 0
AND CHARINDEX('$Assy', ASSY_LINE) > 0";
$result = sqlsrv_query($conn, $query);

$display_count = 0;

while($rows=sqlsrv_fetch_array($result)){

    

?>

<div>

    <div class="receive_div margin_bottom">

        <div class="margin_bottom">
            <span class='receive_label' id="date"> <?php echo $MonthName . " " . $currentDay . ", " . $currentYear; ?> </span>
        </div>

        <div class="CONTAINER margin_bottom">

            <!-- GOODS CODE -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Goods Code: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='goods_code'>" . $rows['GOODS_CODE'] . "</span>";
                    ?>
                </div>
            </div>

            <!-- SUPPLIER -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Supplier: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='supplier'>" . $Vender_name . "</span>";
                    ?>
                </div>
            </div>

            <!-- MAKER -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Maker: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='maker'>" . $rows['MANUFACTURER_NAME'] . "</span>";
                    ?>
                </div>
            </div>

            <!-- ITEM CODE -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Item Code: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='item_code'>" . $rows['ITEM_CODE'] . "</span>";
                    ?>
                </div>
            </div>

            <!-- PART NUMBER -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'>MS21 Part Number: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='part_num'>" . $rows['PART_NUMBER'] . "</span>";
                    ?>
                </div>
            </div>

            <!-- PART NUMBER -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'>Supplier Part Number: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='goods_name'>" . $Goods_name . "</span>";
                    ?>
                </div>
            </div>

            <!-- PART NAME -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Part Name: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        // echo "<span class='receive_content container margin_bottom' id='part_name'>" . $rows['FD_NAME'] . "</span>";
                        echo "<span class='receive_content container margin_bottom' id='part_name'>" . $Goods_name_2 . "</span>";
                    ?>
                </div>
            </div>

            <!-- P.O QUANTITY -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'>P.O Quantity: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='PO_QTY'>" . $Po_Qty . "</span>";
                    ?>
                </div>
            </div>

            <!-- P.O BALANCE -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'>P.O Balance: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='PO_BAL'>" . $Po_Bal . "</span>";
                    ?>
                </div>
            </div>

            <!-- TOSS P.O -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> TOSS P.O: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='PO_input'>" . $Po . "</span>";
                    ?>
                </div>
            </div>

            <!-- MS21 P.O -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> MS21 P.O: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom'>" . $MS_Po . "</span>";
                    ?>
                </div>
            </div>

            <!-- DELIVERY DATE -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> P.O Delivery Date: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom'>" . $D_Date . "</span>";
                    ?>
                </div>
            </div>

            <!-- ASSY LINE -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Assy Line: </div>
                </div>
                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='assy_line'>" . $rows['ASSY_LINE'] . "</span>";
                    ?>
                </div>
            </div>

            <!-- QUANTITY -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Quantity: </div>
                </div>
                <div class="col-1-65">
                    <input type="number" class="receive_input" id="quantity_input" value="">
                </div>
            </div>

            <!-- INVOICE -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Invoice No: </div>
                </div>
                <div class="col-1-65">
                    <input type="text" class="receive_input" id="invoice_input" value="">
                </div>
            </div>

            <!-- Traceability QR Code Generator -->
            <!-- Total Number of Boxes -->
            <div class="container flex">
                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Total Number of Boxes: </div>
                </div>
                <div class="col-1-65">
                    <input type="number" class="receive_input" id="totalNumberOfBox_input" value="">
                </div>
            </div>
            <!-- Traceability QR Code Generator -->
        </div>
        
        <button class="receive_button" id="receive_btn">
            <!-- Traceability QR Code Generator -->
            <div class="printBtnSpinner" style="margin-left:10px; margin-right:10px; display:none;">
            </div>
            <!-- Traceability QR Code Generator -->
                RECEIVE
        </button>
    </div>
        
        

</div>

    <div class="receive_div close">

        <div class="margin_bottom close">
            <span class='receive_label'> Purchase Slip: </span> <br>
            <input type="text" class="receive_input" id="purchase_slip" value="IM<?php echo $currentYear . $currentMonth ?>-000">
        </div>

        <div class="margin_bottom close">
            <span class='receive_label'> Sales Slip: </span> <br>
            <input type="text" class="receive_input" id="sales_slip" value="INV<?php echo $currentYear . $currentMonth ?>-000"> 
        </div>

        <div class="margin_bottom">
            
        </div>

    </div>

</div>

<?php } ?>

<script> //AJAX PARA SA SEPARATE NA FILE NG PAG CONFIRM SA 7 AM

    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
                
    var CurrentDate = year + '-' + month + '-' + day;

    // jQuery('#quantity_input').blur(function () {
    //     var Name = jQuery(this).val(); //or this.value
    //     console.log(Name);
    // });

    var quantity = '';
    var invoice = '';
    // Traceability QR Code Generator
    var totalNumberOfBox = '';
    // Traceability QR Code Generator

    // var po = $("#PO_input").val();

    jQuery('#quantity_input').blur(function () {
        quantity = jQuery(this).val(); //or this.value
        // console.log(quantity);
    });

    jQuery('#invoice_input').blur(function () {
        invoice = jQuery(this).val(); //or this.value
        // console.log(invoice);
    });

    // Traceability QR Code Generator
    jQuery('#totalNumberOfBox_input').blur(function () {
        totalNumberOfBox = jQuery(this).val(); //or this.value
        // console.log(invoice);
    });
    // Traceability QR Code Generator

    // jQuery('#PO_input').blur(function () {
    //     po = jQuery(this).val(); //or this.value
    //     // console.log(invoice);
    // });
    

    $("#receive_btn").click(function() {
        // Traceability QR Code Generator
        document.getElementById('receive_btn').disabled = true;
        document.getElementById('receive_btn').style.backgroundColor = "gray";
        document.querySelector('.printBtnSpinner').style.display = "block";
        // Traceability QR Code Generator

        var goods_code = document.getElementById('goods_code');
        var goods_code_value = goods_code.textContent;
        // console.log(goods_code_value);

        var supplier = document.getElementById('supplier');
        var supplier_value = supplier.textContent;
        // console.log(supplier_value);

        var item_code = document.getElementById('item_code');
        var item_code_value = item_code.textContent;
        // console.log(item_code_value);

        var material = document.getElementById('part_name');
        var material_value = material.textContent;
        // console.log(material_value);

        var part_num = document.getElementById('part_num');
        var part_num_value = part_num.textContent;
        // console.log(part_num_value);

        var goods_name = document.getElementById('goods_name');
        var goods_name_value = goods_name.textContent;

        var part_name = document.getElementById('part_name');
        var part_name_value = part_name.textContent;
        // console.log(part_name_value);

        // var location = document.getElementById('location');
        // var location_value = location.textContent;
        // console.log(location_value);

        var po_raw = document.getElementById('PO_input');
        var po = po_raw.textContent;

        var assy_line = document.getElementById("assy_line");
        var assy_line_value = assy_line.textContent;
        // console.log(assy_line_value);

        // var quantity = $("#quantity_input").attr('value');
        // console.log(quantity);

        // var invoice = $("#invoice_input").attr('value');
        // console.log(invoice);

        
        var purchase_slip = $("#purchase_slip").val();
        // console.log(purchase_slip);

        var sales_slip = $("#sales_slip").val();
        // console.log(sales_slip);

        $.ajax({
            type: "POST",
            url: '../Ajax_Scanner/upload_receive.php',
            data: {
                goods_code_value: goods_code_value,
                supplier_value: supplier_value,
                item_code_value: item_code_value,
                material_value: material_value,
                part_num_value: part_num_value,
                goods_name_value: goods_name_value,
                part_name_value: part_name_value,
                assy_line_value: assy_line_value,
                quantity: quantity,
                invoice: invoice,
                purchase_slip: purchase_slip,
                sales_slip: sales_slip,
                // location_value: location_value,
                po: po,
                // Traceability QR Code Generator
                totalNumberOfBox: totalNumberOfBox

            },
            cache: false,
            success: function(data) {
                // alert(data);
                $("#QR-Code").val('');
                $("#quantity_input").val('');
                $("#invoice_input").val('');
                // Traceability QR Code Generator
                $("#totalNumberOfBox_input").val('');

                $(".ajax_table_desktop").html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    });

</script>