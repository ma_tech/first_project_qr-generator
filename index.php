<?php 
    include "1Connection.php";
    include "main_function/login.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Warehouse Login</title>

    <!-- LINK PARA SA FONT AWESOME 5 FOR -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" /> -->

    <!-- EXTERNAL CSS -->
    <link rel="stylesheet" href="css/login.css" />


    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/jpg" href="favicon_io/android-chrome-512x512.png"/>

</head>

<body>

        <div class="container">

            <div class="brand-logo"></div>

            <img src="/images/MATech.png" alt=""> <br>

            <span class="brand-title">Sign in to <span class="system-name">Warehouse Receiving</span></span>

            <form class="inputs" name="form" action="main_function/login.php" method="post">
                <input type="text" placeholder="Username" id="ENumber" name="ENumber" required/>
                <input type="password" placeholder="Password" id="PWord" name="PWord" required/>
                <button type="submit" name="button1">LOGIN</button>
            </form>

        </div>
    
</body>

</html>