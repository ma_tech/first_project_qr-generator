<?php

include '../1Connection.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');

if ($currentMonth == "01" || 
    $currentMonth == "03" || 
    $currentMonth == "05" || 
    $currentMonth == "07" || 
    $currentMonth == "08" || 
    $currentMonth == "10" || 
    $currentMonth == "12") {

    $EndDayOfTheMonth = "31";

}

elseif( $currentMonth == "04" || 
        $currentMonth == "06" || 
        $currentMonth == "09" || 
        $currentMonth == "11") {

        $EndDayOfTheMonth = "30";

}

else {

    $EndDayOfTheMonth = "28";

}


$from = $_POST['from'] ?? '';
$to = $_POST['to'] ?? '';
$picked = $_POST['picked'] ?? '';

if($from == '' && $to == '' && $picked == '') {
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$currentYear-$currentMonth-01'
    AND [Req# dlv] <= '$currentYear-$currentMonth-$EndDayOfTheMonth'
    AND [P/O qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to == '' && $picked == ''){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] = '$from'
    AND [P/O qty] > '0'";
}
elseif($from != '' && $to != '' && $picked == '') {
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$from'
    AND [Req# dlv] <= '$to'
    AND [P/O qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from == '' && $to == '' && $picked == 'all'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$currentYear-$currentMonth-01'
    AND [Req# dlv] <= '$currentYear-$currentMonth-$EndDayOfTheMonth'
    AND [P/O qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from == '' && $to == '' && $picked == 'comp'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$currentYear-$currentMonth-01'
    AND [Req# dlv] <= '$currentYear-$currentMonth-$EndDayOfTheMonth'
    AND [P/O qty] > '0'
    AND [P/O balance qty] = '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from == '' && $to == '' && $picked == 'rem'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$currentYear-$currentMonth-01'
    AND [Req# dlv] <= '$currentYear-$currentMonth-$EndDayOfTheMonth'
    AND [P/O qty] > '0'
    AND [P/O balance qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to == '' && $picked == 'all'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] = '$from'
    AND [P/O qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to == '' && $picked == 'comp'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] = '$from'
    AND [P/O qty] > '0'
    AND [P/O balance qty] = '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to == '' && $picked == 'rem'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] = '$from'
    AND [P/O qty] > '0'
    AND [P/O balance qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to != '' && $picked == 'all'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$from'
    AND [Req# dlv] <= '$to'
    AND [P/O qty] > '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to != '' && $picked == 'comp'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] >= '$from'
    AND [Req# dlv] <= '$to'
    AND [P/O qty] > '0'
    AND [P/O balance qty] = '0'
    ORDER BY [Req# dlv] ASC";
}
elseif($from != '' && $to != '' && $picked == 'rem'){
    $query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
    WHERE [Req# dlv] = '$from'
    AND [P/O qty] > '0'
    AND [P/O balance qty] > '0'
    ORDER BY [Req# dlv] ASC";
}

$result = sqlsrv_query($conn_ms21_dup, $query);

$counter = '0';

while($rows=sqlsrv_fetch_array($result)){
    $goods_code[$counter] = $rows['Goods code'];
    $deli_date[$counter] = $rows['Req# dlv']->format('Y-m-d');
    $deli_date_m[$counter] = $rows['Req# dlv']->format('m');
    $toss_po[$counter] = $rows['Slip No'];
    $po_qty[$counter] = $rows['P/O qty'];
    $po_deli[$counter] = $rows['Fin of Pur qty'];
    $po_bal_qty[$counter] = $rows['P/O balance qty'];
    $ms21_po[$counter] = $rows['E/U P/O No'];
    $po_date[$counter] = $rows['P/O date'];
    // $supplier[$counter] = $rows['Cstmr name'];
    $supplier[$counter] = $rows['Vender name'];

    $counter = $counter + 1;
}

?>

<div class="center"><p class="info_area_text"><?php echo $shown; ?></p></div>

<table id="incoming_table" class="text_center fixed">

    <thead>
        <tr>
            <th>Target Delivery Date</th>
            <th>GOODS CODE</th>
            <th>SUPPLIER</th>
            <th>TOSS PO</th>
            <th>PO QTY</th>
            <th>DELIVERED QTY</th>
            <th>PO BALANCE QTY</th>
            <th>MS21 PO</th>
            <th>STATUS</th>
        </tr>
    </thead>

    <tbody>
            
        <?php
            for ($x = 0; $x <= $counter; $x++) {

                $color = "#fff";

                if ($deli_date_m[$x] == '01'){ $bg = "#0000FF"; }
                elseif ($deli_date_m[$x] == '02'){ $bg = "#8F00FF"; }
                elseif ($deli_date_m[$x] == '03'){ $bg = "#F47F39"; }
                elseif ($deli_date_m[$x] == '04'){ $bg = "#A2B2AC"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '05'){ $bg = "#4C9A2A"; }
                elseif ($deli_date_m[$x] == '06'){ $bg = "#0D0C12"; }
                elseif ($deli_date_m[$x] == '07'){ $bg = "#FFC0CB"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '08'){ $bg = "#964B00"; }
                elseif ($deli_date_m[$x] == '09'){ $bg = "#FED758"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '10'){ $bg = "#7AD7F0"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '11'){ $bg = "#FFFDFA"; $color = "#000"; }
                else{$bg = "#880808";}

                if ($po_bal_qty[$x] > 0) { $stats = "REMAINING";  $background = "#800000";}
                else { $stats = "COMPLETED"; $background = "#569F87";}

                if($goods_code[$x] != ''){
                    echo "<tr>
                            <td style='color:".$color."; background:".$bg.";'>" . $deli_date[$x] . "</td>
                            <td>" . $goods_code[$x] . "</td>
                            <td>" . $supplier[$x] . "</td>
                            <td>" . $toss_po[$x] . "</td>
                            <td>" . number_format($po_qty[$x]) . "</td>
                            <td>" . number_format($po_deli[$x]) . "</td>
                            <td>" . number_format($po_bal_qty[$x]) . "</td>
                            <td>" . $ms21_po[$x] . "</td>
                            <td><span  class='badge' style='background:".$background.";'>" . $stats . "</span></td>
                        </tr>";

                }
            }
        ?>

    </tbody>

</table>


<!-- SCRIPT FOR DATATABLE -->
<script>
    $('#incoming_table').DataTable({ 
        order: [[2, 'asc']],
        "pagingType": "simple_numbers", //paging type numbers/simple/simple_numbers/full/full_numbers/first_last_numbers
        "searching": false
    });
</script>