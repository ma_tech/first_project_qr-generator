<?php
    include '../main_function/login.php';  
    if(!isset($_SERVER['HTTP_REFERER'])){
        // redirect them to your desired location
        header('location: ../index.php');
        exit;
    }
    session_start();

    if(!isset($_SESSION['EmpNum'])){
        header("Location:../index.php");
    }

    if(isset($_POST["SIGNOUT"])){
        session_destroy();
        unset($_SESSION["EmpNum"]);
        unset($_SESSION["FullName"]);
        header("Location: ../index.php");
        
        $_SERVER['HTTP_REFERER'] = "";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Code Page</title>
    <link rel="stylesheet" href="assets/css/main/app.css">
    <link rel="stylesheet" href="assets/css/main/app-dark.css">
    <link rel="stylesheet" href="./css/print_qr.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/> -->
    <script>
    /*to prevent Firefox FOUC, this must be here*/
    let FF_FOUC_FIX;
    </script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script> -->
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery3.5.1.js"></script>
    <script src="DataTables/datatables.min.js"></script>
    <!-- DATEPICKER -->
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

    <!-- <script src="https://kit.fontawesome.com/7ea5798589.js" crossorigin="anonymous"></script> -->

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/jpg" href="../favicon_io/android-chrome-512x512.png"/>
</head>
<body style="background-color: #CCCDCF;">

<header>
    <nav>
        <div id="navbar" class="vertical_center" style="background-color: #fff; height:60px;">
            <div class="logo">Warehouse Receiving</div>
        </div>
        <div class="nav-links" style="background-color: #EBEBEB; height:50px;padding-left:30px; ">
            <button class="nav-btn" style="color: #B8B8B8; font-weight: 600;" onclick=mainPage()>Receiving</button>
            <form action="" method="post">
                <button name="SIGNOUT" class="nav-btn logout-btn">Logout<i class="fa-solid fa-power-off" style="margin-left: 5px;"></i></button>
            </form>
            <span style="margin-left:auto; margin-right:10px; font-weight: bold; color: #3c5393">
                <?php echo $_SESSION['FullName']; 
                    
                ?>
                <div class="text-center" style="font-size: 10px;">
                    <?php 
                        if($_SESSION['Admin'] == true){
                            echo "ADMIN";
                        }elseif($_SESSION['Admin'] == false){
                            echo "STAFF";
                        }
                    ?>
                </div>
            </span>
            <span class="rounded-circle" style="margin-right: 30px; border: 2px solid #3c5393;width:40px;height:40px;display:flex; align-items:center;justify-content:center;color: #3c5393;">
                <i class="fa-solid fa-user-tie fa-xl"></i>
            </span>
        </div>
    </nav>
</header>
<div class="container-fluid text-center mx-auto">
    <div class="row mt-3 w-100 mx-auto" style="background-color: #CCCDCF;">
        <div class="row mx-auto mb-3">
            <div id="table-data" class="col-sm-12 mt-4 mb-3" >
                    <table id="data-table" class="table table-bordered" style="background-color:white;min-width: 600px;max-width:100%; box-shadow: 0px 3px 10px #888888;">
                        <thead>
                            <tr style="background-color: #3C5393; color:white;">
                                <th class="text-center">BOX SERIAL NO</th>
                                <th class="text-center">PO</th>
                                <th class="text-center">GOODS CODE</th>
                                <th class="text-center" style="word-break:normal;max-width: 70px;">ITEM CODE</th>
                                <th class="text-center">ASSY LINE</th>
                                <th class="text-center">ENCODE DATE</th>
                                <th class="text-center">ACTUAL RECEIVED DATE</th>
                                <th class="text-center" style="max-width: 70px;">INVOICE</th>
                                <th class="text-center">QUANTITY</th>
                                <th class="text-center" style="word-break:normal;max-width: 50px;">NUMBER OF BOX</th>
                                <th class="text-center">SPQ</th>
                                <th class="text-center" style="word-break:normal;max-width: 80px;">BOX LABEL QR</th>
                                <th class="text-center" style="word-break:normal;max-width: 80px;">INNER LABEL QR</th>
                                
                            </tr>
                        </thead>
                        
                        <!-- <tbody id="dataTable"></tbody> -->
                        <tfoot>
                        <tr><td colspan="13">
                                <button class="btn btn-success " id="printQR" disabled>
                                    <div class="printBtnSpinner" style="display:none"></div>
                                    <span class="fa-solid fa-print fa-xl" style="vertical-align: middle; position:relative;">
                                        <span id="qrTotalPieces" style="position:absolute;top:-25px;left:15px;font-size:14px;" class="badge badge-light"></span>
                                    </span>
                                </button>
                                <button class="btn btn-success " id="printQRFloat" disabled>
                                    <div class="printBtnSpinner" style="display:none"></div>
                                    <span class="fa-solid fa-print fa-xl" style="vertical-align: middle; position:relative;">
                                        <span id="qrTotalPiecesFloat" style="position:absolute;top:-25px;left:15px;font-size:14px;" class="badge badge-light"></span>
                                    </span>
                                </button>
                            </td></tr>
                        </tfoot>
                    </table>
                
                    <center>
                        <div id="spinnerBar" class="spinner-border mt-5" style="width: 3rem; height: 3rem;" role="status">
                            <span class="visually-hidden">Loading...</span>
                            
                        </div>
                    </center>
                <!-- </div> -->
            </div>
            <!-- <p>Date: <input type="text" id="datepicker"></p> -->
        </div>
    </div>
        <div id="blackAndwhiteSpinner" style="position:fixed; width:100%; height:100%;background-color:rgba(255,255,255,0.7); z-index:2; top:0; display:none">
            <div style="background-color:rgba(128,128,128,0.9); width:300px; height:200px;position: fixed;
                    top: 50%;
                    left: 50%;
                    margin-top: -100px;
                    margin-left: -150px;
                    display:flex;
                    justify-content:center;
                    align-items:center;
                    border-radius:10px;">
                <div class="spinner"></div>
            </div>
        </div>
</div>
<!-- <div id="testContainer"></div> -->
<script>
    let testLogger = ()=>{
        // console.log('working')
    }
    // window.addEventListener('scroll', function(){
    //     console.log(document.getElementById("printQR"))
    // })

    let mainPage = ()=>{
        location.href = "../main_page.php"
    }
    var aData = [];
    var innerLabelData = [];
    // console.log("sadfasd")
    let search = ()=> 
    {
        // document.getElementById('dataTable').innerHTML = "";
        // document.getElementById('table-data').style.display = "none"
        // document.getElementById('spinnerBar').style.display = "block"
        // if(document.getElementById('part_number').value !== ""){
        // $(document).ready(function () {
            return fetch('./api/qr_details.php', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            // body: JSON.stringify({
            //     "data": document.getElementById('part_number').value
            // })
            })

            .then(res => {
                
                return res.json()
            })
            .then(result => {
                // checkDuplicate(result)
                // console.log(result[0].BOXLABEL_CHECKBOX)
                document.getElementById('table-data').style.display = "block"
                document.getElementById('spinnerBar').style.display = "none"
                var table = $('#data-table').DataTable({
                    scrollX:true,
                    "drawCallback": function( settings ) {
                        // $( function() {
                            $( "input[type = text]" ).datepicker({
                                dateFormat: 'yy-mm-dd'
                            });
                            $('#data-table tbody tr').on('change', 'input[type = text]', function () {
                                Swal.fire({
                                position: 'center',
                                title: 'Loading...',
                                
                                didOpen: () => {
                                    Swal.showLoading()
                                },
                                showConfirmButton: false,
                                
                                })
                                // console.log("date changed!")
                                var table = $('#data-table').DataTable();
                                var data = table.row($(this).parents('tr')).data();
                                // console.log($(this).closest('tr').find('.actualReceiveDate').prop("value"))
                                // console.log(data.ID)
                                update_actual_receiveDate(data.ID,$(this).closest('tr').find('.actualReceiveDate').prop("value"))
                            })
                        // } );
                    },
                    "order": [[ 0, "desc" ], [ 6, "desc" ]],
                    select: true,
                    data: result,
                    columns: [
                        { data: "BOX_SERIAL_NO" },
                        { data: "PO" },
                        { data: "GOODS_CODE" },
                        { data: "ITEM_CODE" },
                        { data: "ASY_LINE" },
                        { data: "DATE_RECEIVE" },
                        { data: "ACTUAL_RECEIVE_DATE",
                            render: (data,type,row)=>{
                                return `<input style="border:0;color:black; width:80%;font-size:small;" type="text"  name="actualReceiveDate" class="actualReceiveDate" value="${data}" disabled>` 
                                <?php if($_SESSION['Admin'] == true){ ?>
                                    +`<button style="height: 25px; width:25px;border-radius:15px;background: linear-gradient(180deg, #3c5393 10%, #468CCF 100%); color:white;" class="updateReceiveDate" name="updateReceiveDate"><i class="far fa-edit"></i>
                                    </button>`
                                <?php } ?>
                            }
                        },
                        { data: "INVOICE" },
                        { data: "QTY" },
                        { data: "NUMBER_OF_BOX" },
			{ data: "SPQ" },
                        <?php if($_SESSION['Admin'] == false){ ?>
                        { data: "isPrinted",
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input boxLabelQR' type='checkbox' value='' name='boxLabelQR'></div>`,
                            orderable: false,
                            render: function(data,type,row){
                                // console.log(type)
                                if(data == 1){
                                    // console.log("tested")
                                    return `<span class='badge badge-pill badge-success'>Printed by ${row['BOX_QR_PRINTED_BY']}</span>`
                                }
                            },
                        },
                        { data: "INNER_QR_LABEL",
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input innerLabelQR' type='checkbox' value='' name='innerLabelQR'></div>`,
                            orderable: false,
                            render: function(data,type,row){
                                if(data == 1){
                                    // console.log("tested")
                                    return `<span class='badge badge-pill badge-success'>Printed by ${row['INNER_QR_PRINTED_BY']}</span>`
                                }
                            },
                        },
                        <?php }elseif($_SESSION['Admin'] == true){ ?>
                        { data: null,
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input boxLabelQR' type='checkbox' value='' name='boxLabelQR'></div>`,
                        },
                        { data: null,
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input innerLabelQR' type='checkbox' value='' name='innerLabelQR'></div>`,
                        },
                        <?php } ?>
                    ],
                    
                });
                // console.log(table.columns())
            })
        // });
    }
            

setTimeout(()=>{
    $(document).ready(function(){

        

    });
    
},1000)
let samplePrint = ()=>{

}

// setTimeout(()=>{
//     window.onload = search();
// },500)


let resourcesLoader = ()=>{
        
        $('#data-table tbody').on('click', 'input', function () {
            
            var table = $('#data-table').DataTable();
            // var data = table.row(this).data();
            var data = table.row( $(this).parents('tr') ).data();
                
                if(isNaN(data.QTY % data.SPQ)){
                    data.NumberOfInnerQR = 0
                }else {
                    if(data.QTY % data.SPQ != 0){
                    
                    data.NumberOfInnerQR = Math.trunc(data.QTY / data.SPQ) + 1
                    }else{
                    data.NumberOfInnerQR = data.QTY / data.SPQ
                    }
                }

            let aDataStringified = JSON.stringify(data);
            if($(this).prop("checked") == true){
                
                document.getElementById('printQR').disabled = true
                document.getElementById('printQRFloat').disabled = true
                // console.log("ccccc")
                for (let i = 0; i < document.querySelectorAll('.printBtnSpinner').length; i++){
                    document.querySelectorAll('.printBtnSpinner')[i].style.display = "block"
                }
                for (let i = 0; i < document.querySelectorAll('.fa-print').length; i++){
                    document.querySelectorAll('.fa-print')[i].style.display = "none"
                }
                
                var index
                
                    fetch('./api/get_material_code.php', {
                    method: 'POST',
                    headers: {
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify({
                        "GOODS_CODE": data.GOODS_CODE,
                        "ASY_LINE": data.ASY_LINE
                    })
                    })

                    .then(res => {
                        return res.json()
                    })
                    .then( async result => {
                        // console.log(result)
                        data.MATERIAL_CODE = await result.MATERIAL_CODE
                        
                        for (let i = 0; i < document.querySelectorAll('.printBtnSpinner').length; i++){
                            document.querySelectorAll('.printBtnSpinner')[i].style.display = "none"
                        }
                        for (let i = 0; i < document.querySelectorAll('.fa-print').length; i++){
                            document.querySelectorAll('.fa-print')[i].style.display = "block"
                        }
                        document.getElementById('printQR').disabled = false
                        document.getElementById('printQRFloat').disabled = false   
                    })

                if($(this).prop("name") == "boxLabelQR"){
                    
                        
                    jQuery("#table-data .innerLabelQR").prop("disabled",true);
                    
                    
                    $('body').append(`<div style='margin-left:10px;margin-bottom:10px;display:none;' id='${data.ID}'></div>`)
                    qrc = new QRCode(document.getElementById(data.ID), data.BOX_SERIAL_NO+";"+data.PO+";"+data.GOODS_CODE+";"+data.ITEM_CODE+";"+data.INVOICE);
                    aData.push(data)
                    document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) + Number(data.NUMBER_OF_BOX)
                    document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) + Number(data.NUMBER_OF_BOX)
                }
                if($(this).prop("name") == "innerLabelQR"){
                    // console.log(document.getElementById('printQR').disabled)
                    
                    // document.getElementById('printQR').disabled = false
                    // document.getElementById('printQRFloat').disabled = false
                    // console.log(document.getElementById('printQR').disabled)
                    jQuery("#table-data .boxLabelQR").prop("disabled",true);
                    
                    $('body').append(`<div style='display:none;' id='${data.ID}'></div>`)
                    $('body').append(`<div id="${'barcodeContainer'+data.ID}" style='display:block;justify-content:center;align-items:center; width: 180px;height:30px;'><svg id="${'barcode'+data.ID}" style='width: 180px;height:30px;'></svg></div>`)
                    qrc = new QRCode(document.getElementById(data.ID), data.GOODS_CODE);
                    
                    JsBarcode(`#${"barcode"+data.ID}`, data.ITEM_CODE,{
                        width:1,
                        height:20,
                        displayValue: false,
                        background: "none"
                    });
                    
                    innerLabelData.push(data)

                    document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) + Number(data.NumberOfInnerQR)
                    document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) + Number(data.NumberOfInnerQR)

                }
                
            }else{
                if($(this).prop("name") == "boxLabelQR"){
                    document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) - Number(data.NUMBER_OF_BOX)
                    document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) - Number(data.NUMBER_OF_BOX)
                    if(document.getElementById('qrTotalPieces').textContent == 0){
                        document.getElementById('qrTotalPieces').textContent = ""
                    }
                    if(document.getElementById('qrTotalPiecesFloat').textContent == 0){
                        document.getElementById('qrTotalPiecesFloat').textContent = ""
                    }
                    if($(".boxLabelQR:checked").length == 0){
                        document.getElementById('printQR').disabled = true
                        document.getElementById('printQRFloat').disabled = true
                        jQuery("#table-data .innerLabelQR").prop("disabled",false);
                    }
                    
                    document.getElementById(data.ID).remove();
                    aData.map((item)=>{
                        if(data.ID == item.ID){
                            index = aData.findIndex(el => el.ID == item.ID)
                            aData.splice(index, 1);
                        }
                    })
                    
                }

                if($(this).prop("name") == "innerLabelQR"){
                    if($(".innerLabelQR:checked").length == 0){
                        jQuery("#table-data .boxLabelQR").prop("disabled",false);
                    }
                    document.getElementById(data.ID).remove();
                    document.getElementById("barcodeContainer"+data.ID).remove();
                    innerLabelData.map((item)=>{
                        if(data.ID == item.ID){
                            index = innerLabelData.findIndex(el => el.ID == item.ID)
                            innerLabelData.splice(index, 1);
                        }
                    })

                    document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) - Number(data.NumberOfInnerQR)
                        document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) - Number(data.NumberOfInnerQR)

                        if(document.getElementById('qrTotalPieces').textContent == 0)
                        {
                            document.getElementById('qrTotalPieces').textContent = ""
                        }
                        if(document.getElementById('qrTotalPiecesFloat').textContent == 0)
                        {
                            document.getElementById('qrTotalPiecesFloat').textContent = ""
                        }
                        if(innerLabelData.length === 0){
                            document.getElementById('printQR').disabled = true
                            document.getElementById('printQRFloat').disabled = true
                        }
                }
                
            }
            console.log(aData)
            console.log(innerLabelData)   
            if(aData.length > 0)
            {
                console.log('inner disabled')
                jQuery("#table-data .innerLabelQR").prop("disabled",true);
            }else {
                jQuery("#table-data .innerLabelQR").prop("disabled",false);
            }
            if(innerLabelData.length > 0)
            {
                console.log('outer disabled')
                console.log(innerLabelData)
                jQuery("#table-data .boxLabelQR").prop("disabled",true);
            }else {
                jQuery("#table-data .boxLabelQR").prop("disabled",false);
            }   

        })
            // update-02/08/2023
            $('#data-table tbody').on('click', 'button', function () {
                // console.log($(this).prop("name"))
                // if($(this).prop("name") == "updateReceiveDate"){
                    var table = $('#data-table').DataTable();
                    var data = table.row('tr').data();

                    $('#data-table tbody button').css("display","inline-block")
                    
                    $('#data-table tbody input[type = text]').prop("disabled", true)
                    $('#data-table tbody input[type = text]').css('color','black')
                    $('#data-table tbody input[type = text]').css("border", 0)
                    

                    $(this).closest('tr').find('.actualReceiveDate').prop("disabled", false);          
                    $(this).closest('tr').find('.actualReceiveDate').css('color','red');
                    $(this).closest('tr').find('.actualReceiveDate').css('border','1px solid gray');
                    $(this).closest('tr').find('.actualReceiveDate').css('border-radius','5px');
                    $(this).closest('tr').find('.updateReceiveDate').css("display","none");
                    
                    
                // }
            })
            // update-02/08/2023
            $('#printQR,#printQRFloat').on('click', function(){
                return Swal.fire({
                title: 'Are you sure you want to print these QR(s)?',
                // text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3c5393',
                cancelButtonColor: '#d33',
                confirmButtonText: "Yes, I'm sure!"
                }).then((result) => {
                    // console.log(result.isConfirmed)
                    if (result.isConfirmed === false) {
                        return
                    }
                    // console.log($(".boxLabelQR:checked").length)
                    // return console.log($(".innerLabelQR:checked").length)
                    if($(".boxLabelQR:checked").length > 0){
                        // createBoxLabelQR()
                        var a = window.open('', '', 'height=auto, width=auto, margin=0');
                        a.document.write('<html>');
                        a.document.write('<head>');
                        a.document.write('<link rel="stylesheet" href="css/print_design.css" />')
                        
                        a.document.write('</head>');
                        a.document.write("<body>");
                        // console.log(aData)
                        for(let i=0; i<aData.length; i++){
                            // console.log(aData[i].DATE_RECEIVE)
                            // console.log(typeof(aData[i].DATE_RECEIVE.split("-")[1]))
                            let monthData = aData[i].ACTUAL_RECEIVE_DATE.split("-")[1]
                            // bgColor(monthData)
                            // console.log(bgColor(monthData))
                            updateBoxQR_printStatus(aData[i].ID)
                            // console.log(updateBoxQR_printStatus(aData[i].ID).MESSAGE)
                            // document.getElementById("qrcode-for-print").innerHTML = ""
                            // qrc = new QRCode(document.getElementById("qrcode-for-print"), aData[i].PO+";"+aData[i].GOODS_CODE+";"+aData[i].ITEM_CODE+";"+aData[i].INVOICE);
                            // console.log(aData)
                            // console.log(makeQR(aData[i].PO, aData[i].GOODS_CODE, aData[i].ITEM_CODE, aData[i].INVOICE))
                            // console.log(aData[i].NUMBER_OF_BOX)
                            for(let x=0; x<aData[i].NUMBER_OF_BOX; x++){
                                a.document.write("<div style='display:inline-block;'>");
                                a.document.write("<div style='max-width:259px; display:flex; flex-direction:row; height: 110px; '>");
                                    a.document.write(`<div id='first-row-to-print' style='width:130px;border:1px solid black; background-color:${bgColor(monthData).bgColor}; color:${bgColor(monthData).color}'>`);
                                    // console.log(bgColor().bgColor)
                                        a.document.write("<div style='height: 28px;display:flex; flex-direction:row;justify-content:center; align-items:center;'>")
                                            // a.document.write("<div style='height: 20px;width:65px; border-right:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                            //     a.document.write(x + 1);
                                            // a.document.write("</div>");
                                            // a.document.write("<div style='height: 20px;width:65px;border-left:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                            //     a.document.write(aData[i].NUMBER_OF_BOX);
                                            // a.document.write("</div>")
                                            // a.document.write("<div style='height: 28px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                                a.document.write(aData[i].BOX_SERIAL_NO);
                                            // a.document.write("</div>")
                                        a.document.write("</div>")
                                        
                                        a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                            a.document.write(aData[i].INVOICE);
                                        a.document.write("</div>")
                                        a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                            a.document.write(aData[i].ACTUAL_RECEIVE_DATE);
                                        a.document.write("</div>")
                                        a.document.write("<div style='height: 28px;border-top:1px solid black;display:flex;justify-content:center; align-items:center; font-weight:bold'>")
                                            a.document.write(aData[i].MATERIAL_CODE);
                                        a.document.write("</div>")
                                    a.document.write("</div>")
                                    // a.document.write("<div style='width:130px;border: 1px solid red;'>");

                                    // a.document.write("</div>")
                                    a.document.write("<div style='width:130px;border: 1px solid black;display:flex; flex-direction:column;font-size:10px;'>");
                                        a.document.write("<div style='width:65px;height: 55px;display:flex;margin-top: 7px; margin-left: 2px;'>");
                                            a.document.write(document.getElementById(aData[i].ID).innerHTML);
                                        a.document.write("</div>")
                                        a.document.write("<br>")
                                        a.document.write("<div style='margin-top:5px;'>");
                                            a.document.write(aData[i].PART_NUMBER);
                                        a.document.write("</div>");
                                        a.document.write("<div style='margin-top:auto; font-size:6px;color:red;'>");
                                            a.document.write("*Please do not put a stamp on the QR Code.");
                                        a.document.write("</div>");
                                    a.document.write("</div>")
                                a.document.write("</div>")
                                a.document.write("</div>");
                            }
                        }
                        
                        a.document.write('</body></html>');
                        // setTimeout(()=>{
                            
                        // }, 2000)
                        
                        // 
                        setTimeout(()=>{
                            a.print();
                            // console.log(a.onunload)
                            // a.close()
                            location.reload(); 
                            Swal.fire({
                            icon: 'success',
                            // title: 'Hooray',
                            text: 'QR code is being printed!',
                            })
                        }, 1000)
                    }
                    // console.log($(".innerLabelQR:checked").length)
                    // console.log($(".boxLabelQR:checked").length)
                    if($(".innerLabelQR:checked").length > 0){
                        // console.log("inner label printing")
                        var b = window.open('', '', 'height=auto, width=auto, margin=0');
                        b.document.write('<html>');
                        b.document.write('<head>');
                        b.document.write('<link rel="stylesheet" href="css/print_design.css" />')
                        
                        b.document.write('</head>');
                        b.document.write("<body>");
                        
                        // console.log(innerLabelData[i].NumberOfInnerQR)
                        for(let i=0; i < innerLabelData.length; i++){
                            
                            let monthData = innerLabelData[i].ACTUAL_RECEIVE_DATE.split("-")[1]
                            updateInnerQR_printStatus(innerLabelData[i].ID)
                            for(let x=0; x<innerLabelData[i].NumberOfInnerQR; x++){
                                b.document.write("<div style='display:inline-block;font-size:10px; font-weight:bold;'>");
                                    b.document.write("<div style='width:192px; display:flex; flex-direction:column; height: 96px;border: 1px solid black;margin-top:1px; '>");
                                        b.document.write("<div style='display:flex; flex-direction:row;width: 100%;height:64px;'>")
                                            b.document.write("<div style='width: 64px;height:64px;'>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px'>")
                                                    b.document.write(innerLabelData[i].MATERIAL_CODE)
                                                b.document.write("</div>")
                                                b.document.write("<div id='item_code' style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write(innerLabelData[i].ITEM_CODE)
                                                    
                                                    // console.log(innerLabelData[i].ITEM_CODE.length)
                                                b.document.write("</div>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write(innerLabelData[i].GOODS_CODE)
                                                b.document.write("</div>")
                                            b.document.write("</div>")
                                            b.document.write("<div style='width: 64px;height:64px;'>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px'>")
                                                    b.document.write(innerLabelData[i].INVOICE)
                                                b.document.write("</div>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write(`<div style='width: 55px;height:18px;background-color:${bgColor(monthData).bgColor}'>`)
                                                    b.document.write("</div>")
                                                    // console.log(innerLabelData[i].ITEM_CODE.length)
                                                b.document.write("</div>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    // b.document.write((x+1)+"/"+innerLabelData[i].NumberOfInnerQR)
                                                    b.document.write(innerLabelData[i].BOX_SERIAL_NO+"-"+(x+1))
                                                b.document.write("</div>")
                                            b.document.write("</div>")
                                            b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:64px;'>")
                                                b.document.write("<div style='width: 52px;display:flex;height:52px;'>")
                                                    b.document.write(document.getElementById(innerLabelData[i].ID).innerHTML);
                                                b.document.write("</div>")
                                            b.document.write("</div>")
                                        b.document.write("</div>")
                                        b.document.write("<div style='width:100%; height:32px;display:flex;justify-content:center;align-items:center;'>")
                                            b.document.write("<div style='width: 180px;height:30px;display:flex;justify-content:center;align-items:center;'  >")
                                                b.document.write(document.getElementById("barcodeContainer"+innerLabelData[i].ID).innerHTML)
                                                // b.document.write("text")
                                            b.document.write("</div>")
                                        b.document.write("</div>")
                                        
                                    b.document.write("</div>")
                                b.document.write("</div>");
                            }
                        }
                        // console.log(document.getElementById('item_code'))

                        b.document.write('</body></html>');
                        setTimeout(()=>{
                            b.print();
                            // console.log(b)
                            // b.close()
                            location.reload(); 
                            Swal.fire({
                            icon: 'success',
                            // title: 'Hooray',
                            text: 'QR code is being printed!',
                            })
                        }, 1000)
                    }
                    
                })
                
                
            });
        // },1000)
        // })
    }    
    
    search().then(()=>{
        resourcesLoader()
    })
    let createBoxLabelQR = ()=>{
        
    }

let updateBoxQR_printStatus = (dataID)=>{
    return fetch('./api/update_print_status.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": dataID
    })
    })

    .then(res => {
        
        return res.json()
    })
    .then(result => { 
        return result
    })
}

let updateInnerQR_printStatus = (dataID)=>{
    return fetch('./api/update_innerQR_print_status.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": dataID
    })
    })

    .then(res => {
        
        return res.json()
    })
    .then(result => { 
        return result
    })
}

// update-02/08/2023
let update_actual_receiveDate = (dataID,dataDate)=>{
    
    return fetch('./api/change_actual_date.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "id": dataID,
        "ACTUAL_RECEIVE_DATE": dataDate
    })
    })

    .then(res => {
        
        return res.json()
    })
    .then(result => { 
        Swal.fire({
        icon: 'success',
        title: result.MESSAGE,
        text: 'Actual Date has been changed successfully!',
        confirmButtonText: 'OK'
        })
        return location.reload(); 
        // location.reload(); 
        
    })
}
// update-02/08/2023
// bgColor('01')
var bgColor = (month)=>{
    // console.log(month)
    if(month == '01'){
        return {"bgColor":"#0000FF",
                "color": "white"
                }
    }
    else if(month == '02'){
        return {"bgColor":"#8F00FF",
                "color": "white"
                }
    }
    else if(month == '03'){
        return {"bgColor":"#F47F39",
                "color": "black"
                }
    }
    else if(month == '04'){
        return {"bgColor":"#A2B2AC",
                "color": "black"
                }
    }
    else if(month == '05'){
        return {"bgColor":"#4C9A2A",
                "color": "white"
                }
    }
    else if(month == '06'){
        return {"bgColor":"#0D0C12",
                "color": "white"
                }
    }
    else if(month == '07'){
        return {"bgColor":"#FFC0CB",
                "color": "black"
                }
    }
    else if(month == '08'){
        return {"bgColor":"#964B00",
                "color": "white"
                }
    }
    else if(month == '09'){
        return {"bgColor":"#FED758",
                "color": "black"
                }
    }
    else if(month == '10'){
        return {"bgColor":"#7AD7F0",
                "color": "black"
                }
    }
    else if(month == '11'){
        return {"bgColor":"#FFFDFA",
                "color": "black"
                }
    }
    else{
        return {"bgColor":"#880808",
                "color": "white"
                }
    }
// document.getElementById("color-container").textContent = ""
// document.getElementById("goodsCode-container").textContent = document.getElementById('goods_code').textContent
        
}
// window.addEventListener('afterprint', (event) => {
//   alert('After print');
// });
// window.addEventListener('afterprint', (event) => { alert("printed") });
// checkDuplicate();
// const checkDuplicate = (nums) => {
//     // console.log(nums)
// //   nums.sort(); // alters original array
//   let ans = []

//   for(let i = 0; i< nums.length; i++){
//     for(let x = 0; x < nums.length; x++){
//         if(nums[i].PO === nums[x].PO && nums[i].GOODS_CODE === nums[x].GOODS_CODE && nums[i].ITEM_CODE === nums[x].ITEM_CODE && nums[i].INVOICE === nums[x].INVOICE && nums[i].RCVD_DT === nums[x].RCVD_DT){
//             // console.log(nums[i].PO)
//             console.log('duplicate')
//         }
//     }
//   }
// }

let getSPQ = ()=>{
    fetch('./api/get_spq.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": "AM000003"
    })
    })
    .then(res => {
        return res.json()
    })
    .then((result)=>{
        // console.log(result)
    })
}   

window.addEventListener('scroll', function(e){
    var printQRButton = document.getElementById('printQR')
    var printQRButtonFloat = document.getElementById('printQRFloat')
    var bounding = printQRButton.getBoundingClientRect();

    if (
	bounding.top >= 0 &&
	bounding.left >= 0 &&
	bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
	bounding.bottom <= (window.innerHeight + 30 || document.documentElement.clientHeight + 30)
    )
    {
        // console.log('In the viewport!');
        // printQRButtonFloat.style.bottom = "-60px"
        printQRButtonFloat.style.right = "-100px"
        // printQRButtonFloat.style.display = "none"
    } else {
        // console.log('Not in the viewport... whomp whomp');
        // printQRButtonFloat.style.bottom = "10px"
        printQRButtonFloat.style.right = "10px"
        // printQRButtonFloat.style.display = "flex"
        
    }

})
window.onload =  function(e){
    var printQRButton = document.getElementById('printQR')
    var printQRButtonFloat = document.getElementById('printQRFloat')
    var bounding = printQRButton.getBoundingClientRect();

    if (
	bounding.top >= 0 &&
	bounding.left >= 0 &&
	bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
	bounding.bottom <= (window.innerHeight + 30 || document.documentElement.clientHeight + 30)
    )
    {
        // console.log('In the viewport!');
        // printQRButtonFloat.style.bottom = "-60px"
        printQRButtonFloat.style.right = "-100px"
        // printQRButtonFloat.style.display = "none"
    } else {
        // console.log('Not in the viewport... whomp whomp');
        // printQRButtonFloat.style.bottom = "10px"
        printQRButtonFloat.style.right = "10px"
        // printQRButtonFloat.style.display = "flex"
        
    }

}

// Select the table element you want to observe
const table = document.querySelector('table');

// Create a new MutationObserver instance
const observer = new MutationObserver(function(mutationsList, observer) {
  // Iterate over each mutation
  for(let mutation of mutationsList) {
    if (mutation.type === 'childList') {
        if($(this).prop("checked") == true){       
            if($(this).prop("name") == "boxLabelQR")
                {
                    jQuery("#table-data .innerLabelQR").prop("disabled",true);
                }
            else if($(this).prop("name") == "innerLabelQR")
                {
	                jQuery("#table-data .boxLabelQR").prop("disabled",true);
                }
        }
        
    }
  }
});

// Start observing the table for changes
observer.observe(table, { childList: true, subtree: true });


// console.log(table)
// Select the DataTables table you want to observe
// var pageTable = $('#data-table').DataTable();
// Listen for the 'draw.dt' event, which fires whenever the table is redrawn
$('#data-table').on('draw.dt', function() {
  console.log('Table page changed');
  
    if(aData.length > 0)
        {
            console.log('inner disabled')
            jQuery("#table-data .innerLabelQR").prop("disabled",true);
        }else {
            jQuery("#table-data .innerLabelQR").prop("disabled",false);
        }
        if(innerLabelData.length > 0)
        {
            console.log('outer disabled')
            console.log(innerLabelData)
            jQuery("#table-data .boxLabelQR").prop("disabled",true);
        }else {
            jQuery("#table-data .boxLabelQR").prop("disabled",false);
        }   
    
});

// $('.updateReceiveDate').click(() => {
//   swal({
//     title: 'Date picker',
//     html: '<input id="datepicker"></input>',
//     onOpen: function() {
//     	$('#datepicker').datepicker();
//     },
//     preConfirm: function() {
//       return Promise.resolve($('#datepicker').datepicker('getDate'));
//     }
//   }).then(function(result) {
//     swal({
//       type: 'success',
//       html: 'You entered: <strong>' + result + '</strong>'
//     });
//   });
// });
// $( function() {
//     console.log('nice')
//     $( "#datepicker" ).datepicker();
//   } );
</script>
<!-- barcode cdn -->
<script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.0/dist/JsBarcode.all.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/qrcodejs/1.0.0/qrcode.min.js" integrity="sha512-CNgIRecGo7nphbeZ04Sc13ka07paqdeTu0WR1IM4kNcpmBAUSHSQX0FslNhTDadL4O5SAGapGt4FodqL8My0mA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="./js/print_qr.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script> -->
<!-- <script src="DataTables/datatables.min.js"></script> -->
<script src="assets/js/bootstrap.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>
</html>