<?php 
if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location: ../../index.php');
    exit;
  }
  session_start();
  if(!isset($_SESSION['EmpNum'])){
    header('location: ../../index.php');
  }
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
// header('Access-Control-Allow-Credentials: true');
// header('Access-Control-Max-Age: 86400');
// header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
// header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
include "../connection.php";
$input=file_get_contents("php://input");
$decode=json_decode($input, true);

$data = $decode['data'];
// $data = 'AM000003';

$sqlGetSpq = "SELECT [GOODS_CODE],[SPQ] FROM [dbo].[WH_SPQ] WHERE GOODS_CODE = '$data' AND ARCHIVE = '0'";
$resultItem = sqlsrv_query($connSPQ, $sqlGetSpq);

if($resultItem === false) {
    die( print_r( sqlsrv_errors(), true) );
    }

$rows = sqlsrv_has_rows($resultItem);
if($rows === true){
  while($rowItem = sqlsrv_fetch_array($resultItem, SQLSRV_FETCH_ASSOC)){
      // echo json_encode($rowItem);
      // echo $row['ITEM_CODE'];
      // echo $rowItem['ITEM_CODE'];
      echo json_encode(array('SPQ' => $rowItem['SPQ']), JSON_PRETTY_PRINT);
      // echo "<input type='text' id='itemCodeWh' value='".$rowItem['ITEM_CODE']."'>";
  }
}else{
  echo json_encode(array('SPQ' => ""), JSON_PRETTY_PRINT);
}

sqlsrv_close($conn);