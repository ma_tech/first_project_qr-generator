<?php 
if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location: ../../index.php');
    exit;
  }
  session_start();
  if(!isset($_SESSION['EmpNum'])){
    header('location: ../../index.php');
  }
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
// header('Access-Control-Allow-Credentials: true');
// header('Access-Control-Max-Age: 86400');
// header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
// header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
include "../connection.php";
$input=file_get_contents("php://input");
$decode=json_decode($input, true);

$GOODS_CODE = $decode['GOODS_CODE'];
$ASY_LINE = $decode['ASY_LINE'];


// $data = 'LA0-00025';

if (strpos($ASY_LINE, 'P-TOUCH')) {
  $sqlMaterialCode = "SELECT * FROM [WH_ITEMCODE_2] WHERE GOODS_CODE = '$GOODS_CODE' AND ASSY_LINE LIKE '%PTOUCH%'";
}elseif(strpos($ASY_LINE, 'BIPH')){
  $sqlMaterialCode = "SELECT * FROM [WH_ITEMCODE_2] WHERE GOODS_CODE = '$GOODS_CODE' AND ASSY_LINE LIKE '%BIPH%'";
}
else{
  $sqlMaterialCode = "SELECT * FROM [WH_ITEMCODE_2] WHERE GOODS_CODE = '$GOODS_CODE' AND ASSY_LINE LIKE '%$ASY_LINE%'";
}

//$sqlMaterialCode = "SELECT * FROM [WH_ITEMCODE_2] WHERE GOODS_CODE = '$GOODS_CODE' AND ASSY_LINE = '$ASY_LINE'";



$resultItem = sqlsrv_query($connMaterialCode, $sqlMaterialCode);

if($resultItem === false) {
    die( print_r( sqlsrv_errors(), true) );
    }

  $rows = sqlsrv_has_rows($resultItem);
  if($rows === true){
    while($rowItem = sqlsrv_fetch_array($resultItem, SQLSRV_FETCH_ASSOC)){
        // echo json_encode($rowItem);
        // echo $row['ITEM_CODE'];
        // echo $rowItem['ITEM_CODE'];
        // echo $rowItem;
        
        
          echo json_encode(array('MATERIAL_CODE' => $rowItem['ITEM_CODE']), JSON_PRETTY_PRINT);
        
        
        // echo "<input type='text' id='itemCodeWh' value='".$rowItem['ITEM_CODE']."'>";
    }
  }else{
    echo json_encode(array('MATERIAL_CODE' => ""), JSON_PRETTY_PRINT);
  }

sqlsrv_close($conn);