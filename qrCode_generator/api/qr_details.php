<?php 
if(!isset($_SERVER['HTTP_REFERER'])){
  // redirect them to your desired location
  header('location: ../../index.php');
  exit;
}
session_start();
if(!isset($_SESSION['EmpNum'])){
  header('location: ../../index.php');
}
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

// echo $_SERVER['HTTP_REFERER'];
include "../connection.php";
// $input=file_get_contents("php://input");
// $decode=json_decode($input, true);

// $data = $decode['data'];
// $data = "B57U166";
// $data = $decode['data'];
// $data = "B57U166";
$resultArray = array();
$sql = "SELECT ALL
[Receive].[id],
[Receive].[BOX_SERIAL_NO], 
[Receive].[GOODS_CODE],
[Receive].[ITEM_CODE],
[Receive].[DATE_RECEIVE],
[Receive].[PO],
[Receive].[INVOICE],
[Receive].[QTY],
[Receive].[ASY_LINE],
[Receive].[NUMBER_OF_BOX],
[Receive].[isPrinted],
[Receive].[INNER_QR_LABEL],
[Receive].[BOX_QR_PRINTED_BY],
[Receive].[INNER_QR_PRINTED_BY],
[Receive].[ACTUAL_RECEIVE_DATE],
[Receive].[PART_NUMBER],
[WH_SPQ].[SPQ]
FROM [MA_Receiving].[dbo].[Receive]
FULL OUTER JOIN [dbo].[WH_SPQ] ON REPLACE([dbo].[Receive].[GOODS_CODE],'-','') = [dbo].[WH_SPQ].[GOODS_CODE]
WHERE [DATE_RECEIVE] > '2023-01-01'
AND [dbo].[Receive].[ARCHIVE] = 0
ORDER BY BOX_SERIAL_NO DESC";


// $sql = "WITH DEDUPE AS (
//   SELECT  id, GOODS_CODE, ITEM_CODE, DATE_RECEIVE, PO, INVOICE, QTY, ASY_LINE, NUMBER_OF_BOX, isPrinted
//         , ROW_NUMBER() OVER ( PARTITION BY PO, ITEM_CODE, GOODS_CODE, INVOICE, DATE_RECEIVE ORDER BY DATE_RECEIVE) AS OCCURENCE
//   FROM Receive$
//   )
// SELECT  * FROM DEDUPE
// WHERE
// OCCURENCE = 1";

// $sql = "SELECT * FROM Receive$";

// echo $sql;
// $data = "B57U166";
$result = sqlsrv_query($conn, $sql);
// echo $result;

if($result === false) {
  die( print_r( sqlsrv_errors(), true) );
}
    $i = 0;
    while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      
      // echo $row['PO'];
      // echo json_encode($resultArray, JSON_PRETTY_PRINT) . "<br>";
      $resultArray[$i]['ID'] = $row['id'];
      $resultArray[$i]['BOX_SERIAL_NO'] = $row['BOX_SERIAL_NO'];
      $resultArray[$i]['GOODS_CODE'] = $row['GOODS_CODE'];
      $resultArray[$i]['ITEM_CODE'] = $row['ITEM_CODE'];
      $resultArray[$i]['PART_NUMBER'] = $row['PART_NUMBER'];
      $resultArray[$i]['DATE_RECEIVE'] = $row['DATE_RECEIVE']->format('Y-m-d');
      $resultArray[$i]['PO'] = $row['PO'];
      $resultArray[$i]['INVOICE'] = $row['INVOICE'];
      $resultArray[$i]['QTY'] = $row['QTY'];
      $resultArray[$i]['ASY_LINE'] = $row['ASY_LINE'];
      $resultArray[$i]['NUMBER_OF_BOX'] = $row['NUMBER_OF_BOX'];
      $resultArray[$i]['isPrinted'] = $row['isPrinted'];
      $resultArray[$i]['INNER_QR_LABEL'] = $row['INNER_QR_LABEL'];
      $resultArray[$i]['BOX_QR_PRINTED_BY'] = $row['BOX_QR_PRINTED_BY'];
      $resultArray[$i]['INNER_QR_PRINTED_BY'] = $row['INNER_QR_PRINTED_BY'];
      // $resultArray[$i]['SPQ'] = $row['SPQ'];
      if($row['SPQ'] == NULL || $row['SPQ'] == ""){
        $resultArray[$i]['SPQ'] = 0;
      }
      else{
        $resultArray[$i]['SPQ'] = $row['SPQ'];
        // $resultArray[$i]['ACTUAL_RECEIVE_DATE'] = $row['ACTUAL_RECEIVE_DATE']->format('Y-m-d');
      }
      
      if($row['ACTUAL_RECEIVE_DATE'] == NULL || $row['ACTUAL_RECEIVE_DATE'] == ""){
        $resultArray[$i]['ACTUAL_RECEIVE_DATE'] = "null";
      }
      else{
        $resultArray[$i]['ACTUAL_RECEIVE_DATE'] = $row['ACTUAL_RECEIVE_DATE']->format('Y-m-d');
        // $resultArray[$i]['ACTUAL_RECEIVE_DATE'] = $row['ACTUAL_RECEIVE_DATE']->format('Y-m-d');
      }
      // $resultArray[$i]['MATERIAL_CODE'] = checkMaterialID($row['GOODS_CODE']);
    //   $resultArray[$i]['BOXLABEL_CHECKBOX'] = "&lt;input class='form-check-input boxLabelQR' type='checkbox' value='sample'&gt;";
    //   echo $row['PART_NAME'] . "<br>";
      $i++;
      // header('Content-type: application/json');
      
    }

    echo json_encode($resultArray, JSON_PRETTY_PRINT);
    // echo json_encode($resultArray[0], JSON_PRETTY_PRINT);

sqlsrv_close($conn);







    