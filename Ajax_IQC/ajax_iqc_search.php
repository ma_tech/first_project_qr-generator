<?php

include '../1Connection.php';
include '../main_function/login.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$search = $_POST['search'] ?? '';

$query = "SELECT DISTINCT
inspectiondata.id,
inspectiondata.invoice_no, 
inspectiondata.goodsCode, 
inspectiondata.inspection_type, 
inspectiondata.invoicequant,
tblOverall_Judgement.overall_judgement,
tblOverall_Judgement.MaterialCodeBoxSeqID,
LotNumber.lot_no
FROM ((inspectiondata
INNER JOIN tblOverall_Judgement 
ON inspectiondata.MaterialCodeBoxSeqID = tblOverall_Judgement.MaterialCodeBoxSeqID AND
inspectiondata.invoice_no = tblOverall_Judgement.invoice_no)
INNER JOIN LotNumber ON LotNumber.MaterialCodeBoxSeqID = inspectiondata.MaterialCodeBoxSeqID AND
LotNumber.invoice_no = inspectiondata.invoice_no)
WHERE CHARINDEX ('$search', inspectiondata.invoice_no) > 0
OR CHARINDEX ('$search', tblOverall_Judgement.MaterialCodeBoxSeqID) > 0
ORDER BY inspectiondata.id DESC";
$result = sqlsrv_query($connIQC, $query);


echo "<table id='search_iqc_data' class='table w3-table-all w3-hoverable ui striped table'>";

echo "<thead class='report_table_head'>
        <tr>
            <th>invoice number</th>
            <th>goods code</th>
            <th>product name</th>
            <th>inspection type</th>
            <th>invoice quantity</th>
            <th>status</th>
            <th>lot number</th>
        </tr>
      </thead>";

echo "<tbody class='report_table_body'>";




while($rows=sqlsrv_fetch_array($result)){

                                            
    echo "<tr>
            <td>" . $rows['invoice_no'] . "</td>
            <td>" . $rows['goodsCode'] . "</td>
            <td>" . $rows['MaterialCodeBoxSeqID'] . "</td>
            <td>" . $rows['inspection_type'] . "</td>
            <td>" . $rows['invoicequant'] . "</td>
            <td>" . $rows['overall_judgement'] . "</td>
            <td>" . $rows['lot_no'] . "</td>
        </tr>";
}

echo "</tbody>

</table>";


?>

<!-- SCRIPT FOR DATATABLE
<script>
    $('#search_iqc_data').DataTable({ 
        "ordering": false,
        "lengthChange": false, // disable the length 10/25/50/100
        "pagingType": "simple_numbers", //paging type numbers/simple/simple_numbers/full/full_numbers/first_last_numbers
        "pageLength": 12,
        "searching": false
    });
</script> -->